## Homework 2

**In this homework, we're going to explore files and filesystems**

- Just like last time, start by forking this repo into your own Gitlab.com account (via web), and then cloning that repo to your local machine (e.g.: `git clone git@gitlab.com:<your-username>/hw2.git`).

- I will ask questions as we go, and I expect you to answer them inside this file by using a text editor.
For example:

```markdown
#### [x pts] Fake Section:                      	(my text)
> Questions:               				    		(my text)
    - What kind of file are you reading?    		(my text)
        - It is a markdown file             		(your answer -- added with a text editor)
```

- I will also ask you to take a lot of screenshots.  I don't want/need your whole screen. Find a way to grab a small rectangular portion of your screen, so you can just capture what you need.  
	+ when I ask for screenshots, I will write something like:

	```markdown
	- Here is an instruction step
		+ screencap (capfile.png)
	```
	
	that means, take a screencap of the last step, and save it here in this repo as `capfile.png`.  If you want to get fancy, you can make a directory here called `screenshots` or something and keep all your captures inside.

- also, keep in mind that if one of the arguments in the command I give is a file, then you should be working in a directory that contains that file.  For example, when I run `cat mynewfile`, the `cat` program has to be able to find `mynewfile`. `cat` will not search your system for you.  You have to tell it exactly where that file is.  If I first`cd` to the directory which contains `mynewfile`, then I can just use the command as written.  Otherwise, I have to use `cat /full/path/to/mynewfile` so that `cat` can find it. So, if a command is operating on a file, you should check that either: A) the file is inside your current `$PWD` or B) you give the full path to the file, so the program you're calling can find it.

- several commands may require elevated permissions. If you get an error message something like "bad permissions" or "not readable," try using `sudo` in front of the same command

#### [3 pts] Inspect Megi's Multiboot Image for the Pinephone:

- read the instructions at https://xnux.eu/p-boot-demo/ 

- the files are hosted by Megi at http://dl.xnux.eu/p-boot-multi-2020-11-23/  and someone else is hosting a mirror at http://mirror.uxes.cz/dl.xnux.eu/p-boot-multi-2020-11-23/  (I've found the mirror to be faster).  I will be "seeding" the file via torrent, so maybe you can get it from me :)

- each of those directories contain three files:
  
  1. SHA256
  
  2. SHA256.asc
  
  3. multi.img.gz

- download all the files (you may use the .torrent link to get `multi.img.gz`)

- move your downloaded files into this `hw2` directory

> Questions:
>- What kind of file did you just download?
answer - The .gz file is an archive file compressed by the standard GNU zip (gzip)compression algorithm
The .asc file is Action script file or armored ASCII file  which are written in script
>- Why do we call it an "image"?
answer -It is called an "image" as it is used to store image of disc and is commonly used to distribute operating systems. 
>- Why would Megi use this format to distribute it?
answer- Megi used this format as the .img file was pretty large and it will be easier to download in a compressed form. 
Also there are 17 different pinephone distributions, image would make it easier for them to flash the distributions without using multiple uSD cards.
#### [3 pts] .gitignore:
**DO NOT ADD THESE NEW FILES TO THE REPOSITORY -- THEY ARE WAY TOO LARGE (and I already have them)**
  
- you can tell `git` to ignore certain files by making a list of things to ignore inside a new file called `.gitignore`  (remember that the `.` prefix means "hidden"?)
  
- edit the contents of the file with `nano .gitignore`. Make sure it looks like:
    
    ```
    (there may be other stuff here)
    SHA256
    SHA256.asc
    multi.img.gz
    *.img
    *.img.*
    ```
    
    
#### [3 pts] Hashes:

- use `cat SHA256` to see the contents of `SHA256`
    
	- save a screencap as "catsha.png"

- run the command `sha256sum multi.img.gz`
  
	- save a screencap as "shasum.png"

> Questions:
>   
>   - How do the output of the last two commands compare?
answer - Firstly, cat SHA256 command ran alot faster than sha256sum multi.img.gz. 
secondly, sha256sum was utilized to verify the data integrity and autheticity.cat SHA256 shows both the .img.gz and .img files where as the second command only shows .img.gz file
>   - Why would MEGI distribute the `SHA256` file with the image?  
> answer- It was done to verify the authenticity of the file after download. 


#### [3 pts] Look at the Multiboot image (download file):

- use the command `head` to show you the beginning of the file (contents)
	- screencap (headimggz.png)

> Questions:
> 	- Why does it look funny?  (use `head` to look at this .md file for comparison)
answer- It looks like unknown characters that is definitely not english compared to .md file which gives an output of the first 
section of the README file (as much of it that can fit in the screen) and the output is in english. 

#### [6 pts] Extract the actual image from what you downloaded

- use `zcat multi.img.gz > multi.img` to decompress the image file
 
- take a look at the head of the file with `head multi.img`
	+ screencap (headimg.png)

- take a look at the head in a hexdump format:
	+ `head multi.img | xxd`
	+ screencap (headimgxxd.png)
	
> ##### Questions:
> - what does "`zcat`" mean (look it up)?
answer - it is used for viewing the contents of a compressed file without literally uncompressing it.
> - what happens after running this command?
answer -The multi.img file is created. It looks like the decompressed version of multi.img.gz file. 
> - what do the contents of the new file look like as compared to the *.gz version?
answer - The new file also shows strange symbols/characters but head multi.img | xxd just shows a list of oraganized numbers. 
Checking command head multi.img.gz | xxd also shows a list of data similar to that for .img file except this list is significantly shorter.
#### [6 pts] Deeper inspection of image file

- run `sha256sum multi.img` on the new file
	+ screencap (shasumimg.png)

- use `file multi.img` to see if we can learn anything about the file
	+ screencap (filemulti.png)

- use `fdisk -l multi.img` to get more info about the file system on the image
	+ screencap (dumpemulti.png)

>Questions:
> - what do we know about the decompressed file `multi.img` ?
answer -We know that the multi.img is a Linux type file with 2 partition, Disk size of 9.78 GiB, 10485760000 bytes and 20480000 sectors.
> - what does it contain?
answer- It contains 2 Linux type files, 1) multi.img1 has a size of 196M while 2) multi.img2 has a size of 9.6G
> - how did the output of the `sha256sum` command compare to the info in the `SHA256` file?
They are both same but SHA256 gives hashes for both compressed and uncompressed files whereas sha256sum only shows the hash for compressed file
#### [6 pts] Make the image look like a block-device to the system

- run `losetup -f multi.img` to make the file look like a block-device (hard disk) to the system ( you may have to run this as a superuser by prefixing the command with`sudo`)

- run `losetup -a` to get a list of all "loopback" devices (what we just made)

- run `losetup -d /dev/loop##` (##==our number) to remove ("delete") our loopback device

- run `losetup -a` to confirm it's gone

- make it again :)

- `losetup -a` to see what our loop device number is (/dev/loop##) in case it changed

- find our file in the list
	+ screencap a couple lines of output including the one with our file (losetup.png)

- run `lsblk -f` to "list block-devices" with "full" output

- try `fdisk -l /dev/loop##` again on our new loopback device (replace ## with the correct number from above)

- try `file -s` again on the loopback device:
	+ `sudo file -s /dev/loop##`   (replace ## with the correct number from above)
	
- run `losetup -d /dev/loop##` (##==our number) to remove ("delete") our loopback device (again)

#### [6 pts] Recreate loopback device and scan

We are now going to re-create our loopback device again(!) but this time, we might add the argument `-P` to the `losetup` command.  Let's check the `losetup` manual to see what this will do:

+ `man losetup`  (the `man` command automatically uses `pager` which by default is `less` to view the manual.  use the arrows and `b` and `<space>` to move around and `<q>` to quit.)
	
	+ screencap portion of manual explaining `-P` parameter (manlosetup.png)
	
- Go ahead and do it:
	+ `losetup -fP multi.img`
	+ `losetup -a`

- run `lsblk -f` again, and screencap the portion of output with our newest loopback device


>Questions:
>	- did we get a different response from `file` or `fdisk` when using the virtual (loopback) block device?
Yes, file had more detailed output compared to fdisk
>	- is there anything suspicious about the output of either of those commands?
file is used to determine the types of file whereas fdisk is used to list partion of the device. file -s also treats special files ( block/char files) as ordinary files. 
>	- What happened after we ran `losetup -P ...`??  Why??
It created two partion of our loop1 device so now we have loop1p1 and loop1p2 as -P is the partscan command. 

#### [6 pts] Filesystem info (again)

Now that the block device (and any partitions) exist as virtual devices, let's look at the partitions individually:

- run `lsblk -af` to list all block devices.  Pay attention to our loopback device and make note of its partitions

- let's run `file -s /dev/loop##p#>` and `fdisk -l /dev/loop##p#` again on each partition to see if anything's changed.
	+ screencaps (filespart1.png, filespart2.png, fdiskpart1.png, fdiskpart2.png)


#### [10 pts] Mount the loopback devices

The main directory tree in Linux (the one we navigate with `cd` and `ls`) is a virtual filesystem.  It's the heart --and entirety-- of the whole system.  Any _real_ filesystems found on external drives, etc. have to be "mounted" into the main (virtual) system directory tree before they can be accessed or navigated.

- create a place to mount our devices:
	+ `sudo mkdir /mnt/pb1`
	+ `sudo mkdir /mnt/pb2`

- mount our loopback devices there:
	+ `sudo mount /dev/loop##p1 /mnt/pb1`
	+ `sudo mount /dev/loop##p2 /mnt/pb2`

- list the contents of the partitions:
	+ `ls /mnt/pb1`
	+ `ls /mnt/pb2`
	+ screencap (lspb1.png, lspb2.png)
	
>Questions:
>	- did these commands both succeed?
answer - No
>	- If not, which one failed and why?
answer -sudo mount /dev/loop1p1 /mnt/pb1 fails as it has wrong fs type, bad option, bad superblock on /dev/loop0p1, missing codepage or helper program, or other error.

>	- what is the reason, do you think?
It maybe because it doesnt have actual data and cant be mounted. 
#### [3 pts] Unmount the loopback devices

After adding external filesystems into the main (virtual) system FS, we might want to remove it.

- See a list of all mounted filesystems with `mount`
- Unmount the loopback devices from the system fs with `sudo umount /dev/loop##p#` (yes, it's "umount" and not "unmount")
>Questions:
	- where does the _real_ file system on the loopback device reside?
	the real life system resides in memory and mounting allows us to read it. 
	- what happens to it after we unmount the device?
	It doesnt show anything...maybe the file is removed from list. 
